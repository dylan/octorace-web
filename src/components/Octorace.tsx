import React from 'react';
import Guilds from './Guilds';
import Profiles from './Profiles';
import Login from './Login';
import {
    BrowserRouter as Router,
    Route,
    Switch,
} from 'react-router-dom';


export default class Octorace extends React.Component<any, any> {
    public render() {
        let loggedIn = document.cookie.match('discord_token');
        return (
            <Router>
                <Switch>
                    <Route exact path="/">
                        { loggedIn ? <Guilds/> : <Login/> }
                    </Route>
                    <Route path="/guild/:id" component={Profiles}>
                        { loggedIn ? <Profiles/> : <Login/> }
                    </Route>
                </Switch>
            </Router>
        );
    }
}
